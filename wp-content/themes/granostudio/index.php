<?php
/**
 * Pagina inicial
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio
 */



get_header(); ?>

<div class="content-wrap index"> <!-- content move open menu -->

	<header class="principal">
		<div class="logo col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2"><img src="<?php echo get_template_directory_uri();?>/img/header/menu-logo-blog.png" alt="Grano Studio Blog" class="img-responsive"/></div>
		<div class="mask"></div>
	</header>

	<div class="content container blog-index" role="main"> <!-- content move open menu -->
		<!-- Conteúdo Loop -->
		<div class="row text-center">

			<?php
			if( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					/* Post */
					?>
					<div class="col-sm-6 col-sm-offset-3 post scroll-ani" data-anitype="fadeInUp">
						<?php
						//post thumbnail
								if ( has_post_thumbnail() ) {
									$thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
									?>
									<a href="<?php echo get_permalink(); ?>" class="thumb-post" >
										<div class="img-circle thumb" style="background-image:url(<?php echo $thumb_image_url[0]; ?>)"></div>
									</a>
									<?php
								 } else {
									//  echo '<img src=""/>';
								}
						 ?>
						<h1><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
						<?php the_excerpt(); ?>
						<p><a class="btn btn-primary" href="<?php echo get_permalink(); ?>" role="button">Leia mais</a></p>
					</div><!-- /.col-lg-4 -->
					<?php
				}
			} else {
				/* No posts found */
			}
			 ?>

		</div><!-- /.row -->
		<div class="row pageNext">

			<nav>
				<ul class="pager scroll-ani" data-anitype="fadeIn">
					<li><a href="#" class="btn btn-primary" role="button"><span class="icon back"></span><span class="text">Previous</span></a></li>
					<li><a href="#" class="btn btn-primary" role="button"><span class="icon"></span><span class="text">Next</span></a></li>
				</ul>
			</nav>
		</div>
	</div> <!-- /content -->



<?php get_footer(); ?>
