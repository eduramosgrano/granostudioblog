<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Theme Template for Bootstrap</title>

    <!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css"> -->
    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

  </head>

  <body>

    <!-- scroll Top -->
    <div class="totop">
      <div class="line"></div>
    </div>

    <!-- END scroll Top -->

    <!-- Menu principal -->
      <!-- inner menu -->
      <div class="menu-wrap">
				<nav class="menu">
          <div class="menu-content">
            <a href="#">Favorites</a>
						<a href="#">Alerts</a>
						<a href="#">Messages</a>
						<a href="#">Comments</a>
						<a href="#">Analytics</a>
						<a href="#">Reading List</a>
          </div>
          <div class="menu-search">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="txt">Procurar</span><input type="text" name="name" value="" ></input>
          </div>
				</nav>
				<div class="close-button" id="close-button"><div class="line"></div>Close Menu</div>
				<div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
						<path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
					</svg>
				</div>
			</div>
      <!-- hamburguer -->
      <div class="menu-button" id="open-button"><div class="line"></div>Open Menu</div>
    <!-- END Menu principal -->

    <!-- Menu Widget -->
      <!-- inner menu -->
      <div class="widget-wrap">

        <div class="all">
          <div class="item">
            <header>
              <h4>Titulo</h4>
            </header>
            <ul>
              <li>Lista1</li>
              <li>Lista1</li>
              <li>Lista1</li>
            </ul>
          </div>
        </div>

        <div class="close-widget" id="close-widget"><div class="line"></div>Close widget</div>

				<div class="widget">

        </div>

				<div class="morph-shape" id="morph-shape" data-morph-open="M0,0h100c0,0,0,153.6,0,396.2c0,231.4,0,403.8,0,403.8H0c0,0,0-76.8,0-394.9S0,0,0,0z">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
						<path d="M0,0h100c0,0,0,153.6,0,396.2c0,231.4,0,403.8,0,403.8H0c0,0,68.1-76.8,68.1-394.9S0,0,0,0z"/>
					</svg>
				</div>
			</div>
      <!-- hamburguer -->
      <div class="widget-button" id="open-button-widget"><div class="line"></div>Open Widget</div>
      <div class="widget-hover hidden-xs hidden-sm" id="open-hover-widget"></div>

      </div>
    <!-- END Menu widget -->



    <div class="content-wrap search-notfound"> <!-- content move open menu -->

      <header class="principal">
        <div class="mask"></div>
      </header>

      <div class="content container blog-search-notfound" role="main"> <!-- content move open menu -->
        <div class="row page-title">
          <div class="col-sm-6 col-sm-offset-3">
            <h1>Nada encontrado, <br>tente uma nova busca...</h1>

            <form action="" class="form-group">
              <h1 class="glyphicon glyphicon-search" aria-hidden="true"></h1>
              <input type="text" name="name" value="" placeholder="Search" class="form-control"/>
            </form>
          </div>

        </div>
      </div>


      <footer class="principal">

        <div class="coffee">
          <svg version="1.1" id="coffeeSvg" data-morphe="M1280.4,34.7c0,0-132.6-21-287.2-21c-287.2,0-346.2,21-353.2,21c0,0-122-24-434-24c-125.6,0-206,24-206,24V0
	h1280.4V34.7z" data-morphe2="M1280.4,22.7c0,0-132.6,12-287.2,12s-241.3-11-353.2-11s-308.4,11-434,11S0,22.7,0,22.7V0h1280.4V22.7z" data-morphe3="M1280.4,34.7c0,0-132.6-11-287.2-11s-241.3,11-353.2,11s-308.4-14-434-14S0,34.7,0,34.7V0h1280.4V34.7z" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          	 viewBox="0 0 1280 34.7" style="enable-background:new 0 0 1280 34.7;" xml:space="preserve" >
              <path class="st0" d="M1280.4,34.7c0,0-132.6,0-287.2,0s-241.3,0-353.2,0s-308.4,0-434,0s-206,0-206,0V0h1280.4V34.7z"/>
          </svg>
          <svg version="1.1" id="gota" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          	 viewBox="0 0 20 22.8" style="enable-background:new 0 0 20 22.8;" xml:space="preserve">
          <path class="st0" d="M10,1.7c0,0,7.3,8,5.8,13.3C14,21,8,21.7,5.5,18.5C1.9,13.9,10,1.7,10,1.7z"/>
          </svg>

        </div>

        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 text-center">
              <img src="img/footer/cafe.png" alt="" />
              <h3>Assine nossa newsletter</h3>
              <div class="form-center">
                <form class="form-inline">
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                    <div class="input-group">
                      <div class="input-group-addon">@</div>
                      <label for="exampleInputPassword1">Email</label>
                      <input type="text" class="form-control" id="exampleInputAmount" >
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Assinar</button>
                </form>
              </div>

            </div>


            </div>
            <div class="col-xs-12 col-sm-12 copyright text-center ">© 2016-2017 Grano Studio</div>
          </div>
        </div>
      </footer>
    </div> <!-- /content move open menu -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <script src="js/plugins.min.js"></script>
    <script src="js/ui.min.js?012347"></script>
    <!-- <script src="../../assets/js/docs.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
  </body>
</html>
