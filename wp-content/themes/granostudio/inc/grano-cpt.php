<?php

// CLIENTES

  function cpt_clientes() {

    /**
     * Register a custom post type
     *
     * Supplied is a "reasonable" list of defaults
     * @see register_post_type for full list of options for register_post_type
     * @see add_post_type_support for full descriptions of 'supports' options
     * @see get_post_type_capabilities for full list of available fine grained capabilities that are supported
     */
    register_post_type('clientes', array(
      'labels' => array(
        'name'                  => _x( 'Clientes', 'Post type general name', 'twentysixteen' ),
        'singular_name'         => _x( 'Cliente', 'Post type singular name', 'twentysixteen' ),
        'menu_name'             => _x( 'Clientes', 'Admin Menu text', 'twentysixteen' ),
        'name_admin_bar'        => _x( 'Clientes', 'Add New on Toolbar', 'twentysixteen' ),
        'add_new_item'          => __( 'Add New Cliente', 'textdomain' ),
      ),
      'description' => '',
      'public' => true,
      'exclude_from_search' => null,
      'publicly_queryable' => null,
      'show_ui' => true,
      'show_in_nav_menus' => null,
      'hierarchical' => false,
      'supports' => array(
        'title',
        'editor',
        'thumbnail',
      ),
      'capability_type' => 'post',
      'menu_position' => 2,
      'menu_icon' => 'dashicons-id',
      'register_meta_box_cb' => 'add_metaboxes',
    ));
  }
  add_action('init', 'cpt_clientes');

  //add_action( 'add_meta_boxes', 'add_events_metaboxes' );

  add_action( "cmb2_init", "cmb2_clientes" );
  function cmb2_clientes() {
    $prefix = "clientes_";

    //portfolio
    // $cmb->add_field( array(
    //   "name" => "Portfólio",
    //   "desc" => "",
    //   "id"   => "_portfolio",
    //   "type" => "file_list",
    //    // "preview_size" => array( 100, 100 ), // Default: array( 50, 50 )
    // ) );
  }




 ?>
