<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

	<!-- footer -->
	<footer class="principal">

		<div class="coffee">
			<svg version="1.1" id="coffeeSvg" data-morphe="M1280.4,34.7c0,0-132.6-21-287.2-21c-287.2,0-346.2,21-353.2,21c0,0-122-24-434-24c-125.6,0-206,24-206,24V0
	h1280.4V34.7z" data-morphe2="M1280.4,22.7c0,0-132.6,12-287.2,12s-241.3-11-353.2-11s-308.4,11-434,11S0,22.7,0,22.7V0h1280.4V22.7z" data-morphe3="M1280.4,34.7c0,0-132.6-11-287.2-11s-241.3,11-353.2,11s-308.4-14-434-14S0,34.7,0,34.7V0h1280.4V34.7z" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 1280 34.7" style="enable-background:new 0 0 1280 34.7;" xml:space="preserve" >
					<path class="st0" d="M1280.4,34.7c0,0-132.6,0-287.2,0s-241.3,0-353.2,0s-308.4,0-434,0s-206,0-206,0V0h1280.4V34.7z"/>
			</svg>
			<div id="gota">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 20 22.8" style="enable-background:new 0 0 20 22.8;" xml:space="preserve">
				<path class="st0" d="M10,1.7c0,0,7.3,8,5.8,13.3C14,21,8,21.7,5.5,18.5C1.9,13.9,10,1.7,10,1.7z"/>
				</svg>
			</div>

		</div>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 text-center">
					<img src="<?php echo get_template_directory_uri();?>/img/footer/cafe.png" alt="" />
					<h3>Assine nossa newsletter</h3>
					<div class="form-center">
						<form class="form-inline">
							<div class="form-group">
								<label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
								<div class="input-group">
									<div class="input-group-addon">@</div>
									<label for="exampleInputPassword1">Email</label>
									<input type="text" class="form-control" id="exampleInputAmount" >
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Assinar</button>
						</form>
					</div>

				</div>


				</div>
				<div class="col-xs-12 col-sm-12 copyright text-center ">© 2016-2017 Grano Studio</div>
			</div>
		</div>
	</footer>
	</div> <!-- /content move open menu -->


	<?php wp_footer(); ?>


</body>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins.min.js?ver=0002"></script>
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ui.min.js"></script>



<!-- Preloader -->
<script type="text/javascript">
				jQuery('header.principal').css({'visibility':'hidden;'});
    //<![CDATA[
        jQuery(window).on('load', function() { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(350).css({'overflow':'visible'});
						if($('.content-wrap').hasClass('index')){
							$(' header.principal').delay(400).css({'visibility':'visible;'});
							$(' header.principal').delay(400).addClass('animated pulse');
							$('.index header.principal .mask').delay(400).addClass('animated fadeInUp');
						}
						if($('.content-wrap').hasClass('single')){
							$(' header.principal').delay(400).css({'visibility':'visible;'});
							$(' header.principal').delay(400).addClass('animated fadeIn');
							$('.blog-single').delay(400).addClass('animated fadeInUp')
						}

						$('.menu-button').delay(400).addClass('animated fadeInLeftBig');
						$('.widget-button').delay(400).addClass('animated fadeInRightBig');

        })
    //]]>
</script>
</html>
