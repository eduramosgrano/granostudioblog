<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio Sites WP
 */

/**
 * Essense only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'granostudio_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */
function granostudio_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'granostudio' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'granostudio', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'granostudio' ),
		'social'  => __( 'Social Links Menu', 'granostudio' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	// add_theme_support( 'post-formats', array(
	// 	'aside',
	// 	'image',
	// 	'video',
	// 	'quote',
	// 	'link',
	// 	'gallery',
	// 	'status',
	// 	'audio',
	// 	'chat',
	// ) );




	//CUstom POst Type
	include 'inc/grano-cpt.php';
	//custom metabox
	include 'inc/cmb2-conf.php';

	//Custom admin users
	include 'inc/grano-useradmin.php';

	//Custom roles
	include 'inc/grano-roles.php';

	//Admin customize
	include 'inc/grano-admincustomize.php';


	//Require Plgugins com Tgm
	require_once get_template_directory() . '/pluginactivate/class-tgm-plugin-activation.php';
	add_action( 'tgmpa_register', 'cmb2_require_register_required_plugins' );

	function cmb2_require_register_required_plugins() {
		/*
		 * Array of plugin arrays. Required keys are name and slug.
		 * If the source is NOT from the .org repo, then source is also required.
		 */
		$plugins = array(

			// This is an example of how to include a plugin bundled with a theme.
			array(
				'name'               => 'cmb2', // The plugin name.
				'slug'               => 'cmb2', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/cmb2.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				'external_url'       => '', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			),
			array(
				'name'               => 'cmb2-relation', // The plugin name.
				'slug'               => 'cmb2-relation', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/cmb2-relations.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				'external_url'       => '', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			),
			array(
				'name'               => 'ctfrom7', // The plugin name.
				'slug'               => 'ctfrom7', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/contact-form-7.4.4.2.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				//'external_url'       => 'https://downloads.wordpress.org/plugin/contact-form-7.4.5.zip', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			),
			array(
				'name'               => 'WordPress Backup to Dropbox', // The plugin name.
				'slug'               => 'WordPressBackuptoDropbox', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/wordpress-backup-to-dropbox.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				'external_url'       => '', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			)
		);

		//settings
		$config = array(
			'id'           => 'cmb2-require',                 // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => '',                      // Default absolute path to bundled plugins.
			'menu'         => 'tgmpa-install-plugins', // Menu slug.
			'has_notices'  => true,                    // Show admin notices or not.
			'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
			'dismiss_msg'  => 'O Plguin CMB2 não pode ser instalado, instale-o manualmente',                      // If 'dismissable' is false, this message will be output at top of nag.
			'is_automatic' => false,                   // Automatically activate plugins after installation or not.
			'message'      => 'Active agora mesmo o Plgugin CMB2',                      // Message to output right before the plugins table.

		);

		tgmpa( $plugins, $config );
	}


}
endif; // granostudio_setup
add_action( 'after_setup_theme', 'granostudio_setup' );


/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function granostudio_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'granostudio_javascript_detection', 0 );




/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */




	//post format scripts
	function my_enqueue( $hook ) {
    if (is_admin()){
		  // wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js');
    wp_enqueue_script( 'postFormat', get_template_directory_uri() . '/js/essense-postformat.js',  array( 'jquery' ), '20160412', true );
		}
	}

		add_action('admin_enqueue_scripts', 'my_enqueue');

function granostudio_scripts() {

	//Desabilitar jquery
	wp_deregister_script( 'jquery' );

	// Theme stylesheet.
	wp_enqueue_style( 'granostudio-style', get_stylesheet_uri() );

	// Theme front-end stylesheet
	wp_enqueue_style('granostudio-style-front', get_template_directory_uri() . '/css/styles.min.css');


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_localize_script( 'granostudio-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'granostudio' ),
		'collapse' => __( 'collapse child menu', 'granostudio' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts' );


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
