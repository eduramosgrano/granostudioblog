<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div class="content-wrap single"> <!-- content move open menu -->

	<header class="principal">
		<div class="logo col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2"></div>
		<div class="mask"></div>
	</header>

	<div class="content container blog-single" role="main"> <!-- content move open menu -->


		<!-- Conteúdo Loop -->
		<?php if( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				/* Post */?>
				<div class="row">
				<?php
				//  thumbnail
				if ( has_post_thumbnail() ) {
					 $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
					 ?>
					 <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 thumbnail-area"
					 			style="background-image:url(<?php echo $thumb_image_url[0];?>);">
		 			 </div>
					 <?php
				 } else {
					//  echo '<img src=""/>';
				}
				// thumbnail/
				?>

				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 post">
					<div class="blog-post">

						<h1 class="blog-post-title"><?php echo get_the_title(); ?></h1>


						<!-- Author -->
						<div class="author">
								<?php
									$authorFoto = get_user_meta( get_the_author_meta( 'ID' ), "_user_foto", true );
									if(!empty($authorFoto)){
										?>
										<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
											<div class="avatar"
										     	 style="background-image:url(<?php echo $authorFoto?>);">
											 </div>
										</a>
										<?php
									}else{
										?>
										<div class="avatar"></div>
										<?php
									}
								 ?>
								<p>
										por <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author_meta('nickname'); ?></a>
								</p>
						</div>
						<!-- Author/ -->

						<?php
						// categoria
						$categories = get_the_category();
						$cat_id = $categories[0]->term_id;
						if ( $cat_id != 1 ) {
							?>
							<div class="categoria">
									<p>
										<a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>">
											<?php echo esc_html( $categories[0]->name ); ?>
										<svg version="1.1" id="grao" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 viewBox="0 0 228 168" style="enable-background:new 0 0 228 168;" xml:space="preserve">
										<path d="M174,15.8c9.8-0.3,18.4,2.5,25.4,9.6c4.4,4.5,4.7,6.1,1.1,11.3c-12,17.5-28.7,29.5-47.2,39.1
										c-20.5,10.6-42.4,16.8-65.1,19.6c-16.5,2.1-30.5,9.2-42.9,19.7c-7.3,6.3-14,13.3-21,19.9c-4.1,3.9-6,3.4-8.3-1.8
										c-7.2-15.8-5.7-31,3-45.8c11-18.6,27.1-31.9,45.2-42.9c26.6-16.1,55.7-24.1,86.5-26.6C158.4,17.4,166.2,16.6,174,15.8z"/>
										<path d="M77,154.2c-12.4-1.4-24.7-2.8-37.1-4.3c-1.5-0.2-2.9-0.6-4.4-1.1c-9.1-2.7-9.8-5-3.7-12.1c12.6-14.5,28.7-23.6,46.7-29.5
										c14.1-4.6,28.2-9.1,42.5-12.7c29.8-7.5,54.1-23.4,74.3-46.2c2.4-2.7,5-5.4,7.6-8c4.1-4.1,7.6-4.5,10.6,0.4c3,4.9,5.6,10.7,6.3,16.4
										c1.6,14-4.8,25.8-13.5,35.9c-32.5,37.6-72.4,60.6-123.6,60.2c-1.8,0-3.7,0-5.5,0C77.1,153.5,77,153.9,77,154.2z M55.7,126.5
										c-7.3,3.5-14.4,7.3-19.5,13.7c-2.7,3.3-0.2,4.9,2.7,5.5c15.5,3.7,31.2,3.9,45.7,3.1c-10.6-1.5-22.5-3-34.2-5c-7-1.2-7.9-4.6-2.9-9.8
										C50,131.4,52.9,129,55.7,126.5z"/>
										</svg></a>
									</p>
							</div>
							<?php
						}
						// categoria/
						 ?>

						<div style="clear:both" class=" initText" >
						</div>

						<?php the_content(); ?>


					</div><!-- /.blog-post -->



						<footer>
							<!-- admin edit` -->

							<?php
								edit_post_link(
									sprintf(
										/* translators: %s: Name of current post */
										__( 'Editar este post - ', 'granostudio' ),
										get_the_title()
									),
									'<span class="edit-link">',
									'</span>'
								);
							?>

							<!-- /admin edit` -->

							Publicado no dia <?php echo get_the_date(); ?>
						</footer>

				</div><!-- /.col-lg-4 -->


				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 share">
					<a class="face">
						<svg version="1.1" id="Camada_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 19.5 19.5" style="enable-background:new 0 0 19.5 19.5;" xml:space="preserve">
						<path class="st0" d="M12,1.8c-0.8,0-1.5,0.2-2.1,0.7C9.2,3,8.8,3.7,8.6,4.6C8.6,4.9,8.6,5.3,8.5,5.7c0,0.6,0,1.1,0,1.7v0.2H6.1v2.8
							h2.4v7.1h2.9v-7.1h2.4c0.1-0.9,0.2-1.9,0.4-2.8c-0.2,0-0.4,0-0.5,0c-0.7,0-2.2,0-2.2,0s0-1.4,0-2c0-0.8,0.5-1.1,1.2-1.1
							c0.5,0,1,0,1.5,0c0.1,0,0.1,0,0.2,0V1.9c-0.3,0-0.5-0.1-0.8-0.1C13,1.8,12.5,1.8,12,1.8z"/>
						</svg>
					</a>
					<a class="twitter">
						<svg version="1.1" id="Camada_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 19.5 19.5" style="enable-background:new 0 0 19.5 19.5;" xml:space="preserve">
						<path class="st0" d="M16.4,4.9c0.7-0.4,1.1-1,1.4-1.8c0,0,0,0,0,0c0,0,0,0,0,0c-0.6,0.3-1.2,0.6-1.9,0.7c-0.1,0-0.1,0-0.2-0.1
							c-0.8-0.8-1.8-1.1-2.9-0.9c-1,0.2-1.7,0.7-2.2,1.5C10.1,5,10,5.8,10.1,6.5c0,0.1,0,0.2,0.1,0.4c-2.7-0.2-5-1.3-6.8-3.4
							C2.9,4.2,2.8,5,3,5.8c0.2,0.8,0.7,1.5,1.4,2c-0.5,0-1-0.2-1.5-0.4c0,0.6,0.2,1.2,0.5,1.7c0.5,0.8,1.2,1.3,2.1,1.5c0,0,0,0,0,0
							c-0.5,0.1-0.9,0.1-1.4,0c0.3,1,1.3,2.2,3.1,2.3c-0.5,0.4-1,0.7-1.6,0.9c-1,0.4-2.1,0.6-3.3,0.5c0,0,0.1,0,0.1,0.1
							c1.6,1,3.4,1.5,5.3,1.4c0.6,0,1.2-0.1,1.8-0.2c1.8-0.4,3.3-1.2,4.5-2.5c0.7-0.8,1.3-1.6,1.8-2.6c0.4-0.9,0.7-1.9,0.8-2.9
							c0.1-0.5,0.1-1,0.1-1.5c0,0,0-0.1,0.1-0.1c0.4-0.3,0.8-0.7,1.2-1.1c0.1-0.2,0.3-0.3,0.4-0.5c0,0,0,0,0,0C17.7,4.7,17,4.8,16.4,4.9z"
							/>
						</svg>
					</a>
					<a class="link">
						<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve">
									<path d="M381,390.368c-11,0-19,8-19,19v19H38v-232h51c11,0,19-8,19-19s-8-19-19-19H19c-11,0-19,8-19,19
										v270c0,11,8,19,19,19h362c11,0,19-8,19-19v-38C400,398.368,392,390.368,381,390.368z"/>
									<path d="M503,187.368l-140-140c-13-13-32-2-32,14v62c-73,3-131,27-172,68c-63,63-63,146-63,149
										c0,10,8,19,19,19c7,0,13-3,16-9c36-64,131-74,184-74h16v64c0,16,20,26,32,14l140-140c4-4,5-9,5-14S507,191.368,503,187.368z
										 M369,294.368v-35c0-10-7-18-17-19c-1,0-15-2-37-2c-45,0-116,7-169,42c8-20,20-42,40-62c37-37,93-57,164-57c11,0,19-8,19-19v-35
										l93,93L369,294.368z"/>

						</svg>
					</a>
				</div>

			</div><!-- /.row -->

			<!-- comments -->
			<div class="row comments">
				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2" id="logout">
						<?php// comments_template(); ?>
				</div>
			</div>
			<!-- end comments -->

				<?
			}
		} else {
			/* No posts found */
		} ?>





		<!-- paginantion -->
		<div class="row pageNext">

			<nav>
				<ul class="pager">
					<li><a href="#" class="btn btn-primary" role="button"><span class="icon back"></span><span class="text">Previous</span></a></li>
					<li><a href="#" class="btn btn-primary" role="button"><span class="icon"></span><span class="text">Next</span></a></li>
				</ul>
			</nav>
		</div>
	</div> <!-- /content -->


<?php get_footer(); ?>
