/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({



    compass: {                  // Task
      dist: {                   // Target
        options: {
          config: 'config.rb',             // Target options
          sassDir: 'sass',
          cssDir: 'css',
          environment: 'development',
          require: 'bootstrap-sass'
        }
      }
    },

    uglify: {
      my_target: {
        files: {
          '../wp-content/themes/granostudio/js/plugins.min.js': ['js/plugins/jquery-latest.min.js','js/plugins/snap.svg-min.js','js/plugins/ScrollAnimationGrano.js'], //wordpress
          '../wp-content/themes/granostudio/js/ui.min.js': 'js/ui.js', //wordpress
          'js/plugins.min.js': ['js/plugins/jquery-latest.min.js','js/plugins/snap.svg-min.js','js/plugins/ScrollAnimationGrano.js'],
          'js/ui.min.js': 'js/ui.js'
        }
      }
    },

    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          '../wp-content/themes/granostudio/css/styles.min.css': 'css/styles.css', //wordpress
          'css/styles.min.css': 'css/styles.css'
        }
      }
    },

    watch: {
      sass: {
      // We watch and compile sass files as normal but don't live reload here
        files: ['sass/*.scss','js/ui.js','js/plugins/*.js'],
        tasks: ['compass', 'uglify', 'cssmin'],
      }
    }
  });


  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Default task.
  grunt.registerTask('default', ['compass', 'uglify', 'cssmin', 'watch']);

};
