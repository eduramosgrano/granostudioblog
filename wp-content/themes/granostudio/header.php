<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */


?>
<!-- Header -->

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<?php wp_head(); ?>


		<meta property="og:url" content="<?php echo  get_template_directory_uri(); ?>" />
		<meta property="og:title" content="<?php echo get_the_title(); ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:description" content="<?php echo get_the_title(); ?>" />
		<meta property="og:site_name" content="<?php echo get_the_title(); ?>" />
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>logo-essense.jpg" />
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="200" />
		<meta property="og:image:height" content="200" />
		<link rel="shortcut icon" href="<?php echo  get_template_directory_uri();?>/img/e-essense.png">



  </head>

  <body <?php body_class(); ?>>

    <!-- preloader -->
    <div id="preloader">
        <img id="status" src="<?php echo get_template_directory_uri();?>/img/status.svg"/>
    </div>
		<!-- END preloader -->

		<!-- Scroll Animation Grano -->
		<div class="scroll-pointer"></div>
		<!-- END Scroll Animation Grano -->

    <!-- scroll Top -->
    <div class="totop">
      <div class="line"></div>
    </div>

    <!-- END scroll Top -->

    <!-- Menu principal -->
      <!-- inner menu -->
      <div class="menu-wrap">
				<nav class="menu">
          <div class="menu-content">
            <a href="#">Favorites</a>
						<a href="#">Alerts</a>
						<a href="#">Messages</a>
						<a href="#">Comments</a>
						<a href="#">Analytics</a>
						<a href="#">Reading List</a>
          </div>
          <div class="menu-search">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="txt">Procurar</span><input type="text" name="name" value="" ></input>
          </div>
				</nav>
				<div class="close-button" id="close-button"><div class="line"></div>Close Menu</div>
				<div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
						<path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
					</svg>
				</div>
			</div>
      <!-- hamburguer -->
      <div class="menu-button" id="open-button"><div class="line"></div>Open Menu</div>
    <!-- END Menu principal -->

    <!-- Menu Widget -->
      <!-- inner menu -->
      <div class="widget-wrap">

        <div class="all">
          <div class="item">
            <header>
              <h4>Titulo</h4>
            </header>
            <ul>
              <li>Lista1</li>
              <li>Lista1</li>
              <li>Lista1</li>
            </ul>
          </div>
        </div>

        <div class="close-widget" id="close-widget"><div class="line"></div>Close widget</div>

				<div class="widget">

        </div>

				<div class="morph-shape" id="morph-shape" data-morph-open="M0,0h100c0,0,0,153.6,0,396.2c0,231.4,0,403.8,0,403.8H0c0,0,0-76.8,0-394.9S0,0,0,0z">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
						<path d="M0,0h100c0,0,0,153.6,0,396.2c0,231.4,0,403.8,0,403.8H0c0,0,68.1-76.8,68.1-394.9S0,0,0,0z"/>
					</svg>
				</div>
			</div>
      <!-- hamburguer -->
      <div class="widget-button" id="open-button-widget"><div class="line"></div>Open Widget</div>
      <div class="widget-hover hidden-xs hidden-sm" id="open-hover-widget"></div>

      </div>
    <!-- END Menu widget -->

    <!-- END Header -->
