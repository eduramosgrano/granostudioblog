<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div class="row comments">
	<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2" id="logout">


	<?php if ( have_comments() ) : ?>

		<div class="page-header">
			<h3 class="reviews">
				<?php
					$comments_number = get_comments_number();
					if ( 1 === $comments_number ) {
						/* translators: %s: post title */
						printf( _x( '1 Comentário', 'comments title', 'granostudio' ), get_the_title() );
					} else {
						printf(
							/* translators: 1: number of comments, 2: post title */
							_nx(
								'%1$s Comentário',
								'%1$s Comentário',
								$comments_number,
								'comments title',
								'twentysixteen'
							),
							number_format_i18n( $comments_number ),
							get_the_title()
						);
					}
				?>
				</h3>
				<div class="logout">
						<button class="btn btn-warning btn-circle" type="button" onclick="$('#logout').hide(); $('#login').show()">
								<span class="glyphicon glyphicon-off"></span> Logout
						</button>
				</div>
		</div>

		<div class="comment-tabs">
				<ul class="nav nav-tabs" role="tablist">
						<li class="active"><a href="#comments-logout" role="tab" data-toggle="tab"><h5 class="reviews text-capitalize">Comments</h5></a></li>
						<li><a href="#add-comment" role="tab" data-toggle="tab"><h5 class="reviews text-capitalize">Add comment</h5></a></li>
				</ul>



		<div class="tab-content">
				<div class="tab-pane active" id="comments-logout">
						<ul class="media-list">
							<li class="media">
								<a class="pull-left" href="#">
									<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg" alt="profile">
								</a>
								<div class="media-body">
									<div class="well well-lg">
											<h4 class="media-heading reviews">Marco </h4>
											<ul class="media-date reviews list-inline">
												<li class="dd">22</li>
												<li class="mm">09</li>
												<li class="aaaa">2014</li>
											</ul>
											<p class="media-comment">
												Great snippet! Thanks for sharing.
											</p>
											<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
											<a class="btn btn-warning btn-circle  " data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a>
									</div>
								</div>
								<div class="collapse" id="replyOne">
										<ul class="media-list">
												<li class="media media-replied">
														<a class="pull-left" href="#">
															<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/ManikRathee/128.jpg" alt="profile">
														</a>
														<div class="media-body">
															<div class="well well-lg">
																	<h4 class="media-heading   reviews"><span class="glyphicon glyphicon-share-alt"></span> The Hipster</h4>
																	<ul class="media-date   reviews list-inline">
																		<li class="dd">22</li>
																		<li class="mm">09</li>
																		<li class="aaaa">2014</li>
																	</ul>
																	<p class="media-comment">
																		Nice job Maria.
																	</p>
																	<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
															</div>
														</div>
												</li>
												<li class="media media-replied" id="replied">
														<a class="pull-left" href="#">
															<img class="media-object img-circle" src="https://pbs.twimg.com/profile_images/442656111636668417/Q_9oP8iZ.jpeg" alt="profile">
														</a>
														<div class="media-body">
															<div class="well well-lg">
																	<h4 class="media-heading   reviews"><span class="glyphicon glyphicon-share-alt"></span> Mary</h4></h4>
																	<ul class="media-date   reviews list-inline">
																		<li class="dd">22</li>
																		<li class="mm">09</li>
																		<li class="aaaa">2014</li>
																	</ul>
																	<p class="media-comment">
																		Thank you Guys!
																	</p>
																	<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
															</div>
														</div>
												</li>
										</ul>
								</div>
							</li>
						</ul>
				</div>
		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 42,
				) );
			?>
		</ol><!-- .comment-list -->

		<div class="row comments">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2" id="logout">
					<?php the_comments_navigation(); ?>
			</div>
		</div>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentysixteen' ); ?></p>
	<?php endif; ?>

	<?php
		comment_form( array(
			'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title">',
			'title_reply_after'  => '</h2>',
		) );
	?>

</div><!-- .comments-area -->


<!-- <div class="row comments">
	<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2" id="logout">
			<div class="page-header">
					<h3 class="reviews">Leave your comment</h3>
					<div class="logout">
							<button class="btn btn-warning btn-circle" type="button" onclick="$('#logout').hide(); $('#login').show()">
									<span class="glyphicon glyphicon-off"></span> Logout
							</button>
					</div>
			</div>
			<div class="comment-tabs">
					<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#comments-logout" role="tab" data-toggle="tab"><h5 class="reviews text-capitalize">Comments</h5></a></li>
							<li><a href="#add-comment" role="tab" data-toggle="tab"><h5 class="reviews text-capitalize">Add comment</h5></a></li>
					</ul> -->
					<!-- <div class="tab-content">
							<div class="tab-pane active" id="comments-logout">
									<ul class="media-list">
										<li class="media">
											<a class="pull-left" href="#">
												<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg" alt="profile">
											</a>
											<div class="media-body">
												<div class="well well-lg">
														<h4 class="media-heading reviews">Marco </h4>
														<ul class="media-date reviews list-inline">
															<li class="dd">22</li>
															<li class="mm">09</li>
															<li class="aaaa">2014</li>
														</ul>
														<p class="media-comment">
															Great snippet! Thanks for sharing.
														</p>
														<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
														<a class="btn btn-warning btn-circle  " data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a>
												</div>
											</div>
											<div class="collapse" id="replyOne">
													<ul class="media-list">
															<li class="media media-replied">
																	<a class="pull-left" href="#">
																		<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/ManikRathee/128.jpg" alt="profile">
																	</a>
																	<div class="media-body">
																		<div class="well well-lg">
																				<h4 class="media-heading   reviews"><span class="glyphicon glyphicon-share-alt"></span> The Hipster</h4>
																				<ul class="media-date   reviews list-inline">
																					<li class="dd">22</li>
																					<li class="mm">09</li>
																					<li class="aaaa">2014</li>
																				</ul>
																				<p class="media-comment">
																					Nice job Maria.
																				</p>
																				<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
																		</div>
																	</div>
															</li>
															<li class="media media-replied" id="replied">
																	<a class="pull-left" href="#">
																		<img class="media-object img-circle" src="https://pbs.twimg.com/profile_images/442656111636668417/Q_9oP8iZ.jpeg" alt="profile">
																	</a>
																	<div class="media-body">
																		<div class="well well-lg">
																				<h4 class="media-heading   reviews"><span class="glyphicon glyphicon-share-alt"></span> Mary</h4></h4>
																				<ul class="media-date   reviews list-inline">
																					<li class="dd">22</li>
																					<li class="mm">09</li>
																					<li class="aaaa">2014</li>
																				</ul>
																				<p class="media-comment">
																					Thank you Guys!
																				</p>
																				<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
																		</div>
																	</div>
															</li>
													</ul>
											</div>
										</li>
									</ul>
							</div> -->
							<div class="tab-pane" id="add-comment">
									<form action="#" method="post" class="form-horizontal" id="commentForm" role="form">
											<div class="form-group">
													<div class="col-sm-10 col-sm-offset-1">
														<textarea class="form-control well well-lg" name="addComment" id="addComment" rows="5" placeholder="Comment"></textarea>
													</div>
											</div>
											<div class="form-group">
													<div class="col-sm-offset-1 col-sm-10 text-center">
															<button class="btn btn-success btn-circle  " type="submit" id="submitComment"><span class="glyphicon glyphicon-send"></span> Summit comment</button>
													</div>
											</div>
									</form>
							</div>

					</div>
			</div>
</div>
</div>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1" id="login">
			<div class="page-header">
					<h3 class="reviews">Leave your comment</h3>
					<div class="logout">
							<button class="btn btn-default btn-circle  " type="button" onclick="$('#login').hide(); $('#logout').show()">
									<span class="glyphicon glyphicon-off"></span> Login
							</button>
					</div>
			</div>
			<div class="comment-tabs">
					<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#comments-login" role="tab" data-toggle="tab"><h4 class="reviews text-capitalize">Comments</h4></a></li>
							<li><a href="#add-comment-disabled" role="tab" data-toggle="tab"><h4 class="reviews text-capitalize">Add comment</h4></a></li>
							<li><a href="#new-account" role="tab" data-toggle="tab"><h4 class="reviews text-capitalize">Create an account</h4></a></li>
					</ul>
					<div class="tab-content">
							<div class="tab-pane active" id="comments-login">
									<ul class="media-list">
										<li class="media">
											<a class="pull-left" href="#">
												<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg" alt="profile">
											</a>
											<div class="media-body">
												<div class="well well-lg">
														<h4 class="media-heading   reviews">Marco</h4>
														<ul class="media-date   reviews list-inline">
															<li class="dd">22</li>
															<li class="mm">09</li>
															<li class="aaaa">2014</li>
														</ul>
														<p class="media-comment">
															Great snippet! Thanks for sharing.
														</p>
														<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
														<a class="btn btn-warning btn-circle  " data-toggle="collapse" href="#replyThree"><span class="glyphicon glyphicon-comment"></span> 2 comments</a>
												</div>
											</div>
											<div class="collapse" id="replyThree">
													<ul class="media-list">
															<li class="media media-replied">
																	<a class="pull-left" href="#">
																		<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/ManikRathee/128.jpg" alt="profile">
																	</a>
																	<div class="media-body">
																		<div class="well well-lg">
																				<h4 class="media-heading   reviews"><span class="glyphicon glyphicon-share-alt"></span> The Hipster</h4>
																				<ul class="media-date   reviews list-inline">
																					<li class="dd">22</li>
																					<li class="mm">09</li>
																					<li class="aaaa">2014</li>
																				</ul>
																				<p class="media-comment">
																					Nice job Maria.
																				</p>
																				<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
																		</div>
																	</div>
															</li>
															<li class="media media-replied" id="replied">
																	<a class="pull-left" href="#">
																		<img class="media-object img-circle" src="https://pbs.twimg.com/profile_images/442656111636668417/Q_9oP8iZ.jpeg" alt="profile">
																	</a>
																	<div class="media-body">
																		<div class="well well-lg">
																				<h4 class="media-heading   reviews"><span class="glyphicon glyphicon-share-alt"></span> Mary</h4></h4>
																				<ul class="media-date   reviews list-inline">
																					<li class="dd">22</li>
																					<li class="mm">09</li>
																					<li class="aaaa">2014</li>
																				</ul>
																				<p class="media-comment">
																					Thank you Guys!
																				</p>
																				<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
																		</div>
																	</div>
															</li>
													</ul>
											</div>
										</li>
										<li class="media">
											<a class="pull-left" href="#">
												<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kurafire/128.jpg" alt="profile">
											</a>
											<div class="media-body">
												<div class="well well-lg">
														<h4 class="media-heading   reviews">Nico</h4>
														<ul class="media-date   reviews list-inline">
															<li class="dd">22</li>
															<li class="mm">09</li>
															<li class="aaaa">2014</li>
														</ul>
														<p class="media-comment">
															I'm looking for that. Thanks!
														</p>
														<div class="embed-responsive embed-responsive-16by9">
																<iframe class="embed-responsive-item" src="//www.youtube.com/embed/80lNjkcp6gI" allowfullscreen></iframe>
														</div>
														<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
												</div>
											</div>
										</li>
										<li class="media">
											<a class="pull-left" href="#">
												<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/lady_katherine/128.jpg" alt="profile">
											</a>
											<div class="media-body">
												<div class="well well-lg">
														<h4 class="media-heading   reviews">Kriztine</h4>
														<ul class="media-date   reviews list-inline">
															<li class="dd">22</li>
															<li class="mm">09</li>
															<li class="aaaa">2014</li>
														</ul>
														<p class="media-comment">
															Yehhhh... Thanks for sharing.
														</p>
														<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
														<a class="btn btn-warning btn-circle  " data-toggle="collapse" href="#replyFour"><span class="glyphicon glyphicon-comment"></span> 1 comment</a>
												</div>
											</div>
											<div class="collapse" id="replyFour">
													<ul class="media-list">
															<li class="media media-replied">
																	<a class="pull-left" href="#">
																		<img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jackiesaik/128.jpg" alt="profile">
																	</a>
																	<div class="media-body">
																		<div class="well well-lg">
																				<h4 class="media-heading   reviews"><span class="glyphicon glyphicon-share-alt"></span> Lizz</h4>
																				<ul class="media-date   reviews list-inline">
																					<li class="dd">22</li>
																					<li class="mm">09</li>
																					<li class="aaaa">2014</li>
																				</ul>
																				<p class="media-comment">
																					Classy!
																				</p>
																				<a class="btn btn-info btn-circle  " href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
																		</div>
																	</div>
															</li>
													</ul>
											</div>
										</li>
									</ul>
							</div>
							<div class="tab-pane" id="add-comment-disabled">
									<div class="alert alert-info alert-dismissible" role="alert">
										<button type="button" class="close" data-dismiss="alert">
											<span aria-hidden="true">×</span><span class="sr-only">Close</span>
										</button>
										<strong>Hey!</strong> If you already have an account <a href="#" class="alert-link">Login</a> now to make the comments you want. If you do not have an account yet you're welcome to <a href="#" class="alert-link"> create an account.</a>
									</div>
									<form action="#" method="post" class="form-horizontal" id="commentForm" role="form">
											<div class="form-group">
													<label for="email" class="col-sm-2 control-label">Comment</label>
													<div class="col-sm-10">
														<textarea class="form-control" name="addComment" id="addComment" rows="5" disabled></textarea>
													</div>
											</div>
											<div class="form-group">
													<label for="uploadMedia" class="col-sm-2 control-label">Upload media</label>
													<div class="col-sm-10">
															<div class="input-group">
																<div class="input-group-addon">http://</div>
																<input type="text" class="form-control" name="uploadMedia" id="uploadMedia" disabled>
															</div>
													</div>
											</div>
											<div class="form-group">
													<div class="col-sm-offset-2 col-sm-10">
															<button class="btn btn-success btn-circle   disabled" type="submit" id="submitComment"><span class="glyphicon glyphicon-send"></span> Summit comment</button>
													</div>
											</div>
									</form>
							</div>
							<div class="tab-pane" id="new-account">
									<form action="#" method="post" class="form-horizontal" id="commentForm" role="form">
											<div class="form-group">
													<label for="name" class="col-sm-2 control-label">Name</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" name="name" id="name">
													</div>
											</div>
											<div class="form-group">
													<label for="email" class="col-sm-2 control-label">Email</label>
													<div class="col-sm-10">
														<input type="email" class="form-control" name="email" id="email" required>
													</div>
											</div>
											<div class="form-group">
													<label for="password" class="col-sm-2 control-label">Password</label>
													<div class="col-sm-10">
														<input type="password" class="form-control" name="password" id="password">
													</div>
											</div>
											<div class="form-group">
													<div class="checkbox">
															<label for="agreeTerms" class="col-sm-offset-2 col-sm-10">
																	<input type="checkbox" name="agreeTerms" id="agreeTerms"> I agree all <a href="#">Terms & Conditions</a>
															</label>
													</div>
											</div>
											<div class="form-group">
													<div class="col-sm-offset-2 col-sm-10">
															<button class="btn btn-primary btn-circle  " type="submit" id="submit">Create an account</button>
													</div>
											</div>
									</form>
							</div>
					</div>
			</div>
	</div>
</div>
