
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Theme Template for Bootstrap</title>

    <!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css"> -->
    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

  </head>

  <body>

    <!-- scroll Top -->
    <div class="totop">
      <div class="line"></div>
    </div>

    <!-- END scroll Top -->

    <!-- Menu principal -->
      <!-- inner menu -->
      <div class="menu-wrap">
				<nav class="menu">
          <div class="menu-content">
            <a href="#">Favorites</a>
						<a href="#">Alerts</a>
						<a href="#">Messages</a>
						<a href="#">Comments</a>
						<a href="#">Analytics</a>
						<a href="#">Reading List</a>
          </div>
          <div class="menu-search">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="txt">Procurar</span><input type="text" name="name" value="" ></input>
          </div>
				</nav>
				<div class="close-button" id="close-button"><div class="line"></div>Close Menu</div>
				<div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
						<path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
					</svg>
				</div>
			</div>
      <!-- hamburguer -->
      <div class="menu-button" id="open-button"><div class="line"></div>Open Menu</div>
    <!-- END Menu principal -->

    <!-- Menu Widget -->
      <!-- inner menu -->
      <div class="widget-wrap">

        <div class="all">
          <div class="item">
            <header>
              <h4>Titulo</h4>
            </header>
            <ul>
              <li>Lista1</li>
              <li>Lista1</li>
              <li>Lista1</li>
            </ul>
          </div>
        </div>

        <div class="close-widget" id="close-widget"><div class="line"></div>Close widget</div>

				<div class="widget">

        </div>

				<div class="morph-shape" id="morph-shape" data-morph-open="M0,0h100c0,0,0,153.6,0,396.2c0,231.4,0,403.8,0,403.8H0c0,0,0-76.8,0-394.9S0,0,0,0z">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
						<path d="M0,0h100c0,0,0,153.6,0,396.2c0,231.4,0,403.8,0,403.8H0c0,0,68.1-76.8,68.1-394.9S0,0,0,0z"/>
					</svg>
				</div>
			</div>
      <!-- hamburguer -->
      <div class="widget-button" id="open-button-widget"><div class="line"></div>Open Widget</div>
      <div class="widget-hover hidden-xs hidden-sm" id="open-hover-widget"></div>

      </div>
    <!-- END Menu widget -->



    <div class="content-wrap single"> <!-- content move open menu -->

      <header class="principal">
        <div class="logo col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2"><img src="img/header/menu-logo-blog.png" alt="Grano Studio Blog" class="img-responsive"/></div>
        <div class="mask"></div>
      </header>

      <div class="content container blog-single" role="main"> <!-- content move open menu -->
        <!-- Conteúdo Loop -->
        <div class="row">
          <!-- thumbnail -->
          <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 thumbnail-area">

          </div>

          <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 post">
            <div class="blog-post">
              <h1 class="blog-post-title">Sample blog post</h1>

              <div class="author">
                  <div class="avatar">

                  </div>
                  <p>
                    por <a href="#">Edu Ramos</a>
                  </p>
              </div>
              <div class="categoria">
                  <p>
                    <a href="#">Marketing<svg version="1.1" id="grao" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 228 168" style="enable-background:new 0 0 228 168;" xml:space="preserve">
                    <path d="M174,15.8c9.8-0.3,18.4,2.5,25.4,9.6c4.4,4.5,4.7,6.1,1.1,11.3c-12,17.5-28.7,29.5-47.2,39.1
                    c-20.5,10.6-42.4,16.8-65.1,19.6c-16.5,2.1-30.5,9.2-42.9,19.7c-7.3,6.3-14,13.3-21,19.9c-4.1,3.9-6,3.4-8.3-1.8
                    c-7.2-15.8-5.7-31,3-45.8c11-18.6,27.1-31.9,45.2-42.9c26.6-16.1,55.7-24.1,86.5-26.6C158.4,17.4,166.2,16.6,174,15.8z"/>
                    <path d="M77,154.2c-12.4-1.4-24.7-2.8-37.1-4.3c-1.5-0.2-2.9-0.6-4.4-1.1c-9.1-2.7-9.8-5-3.7-12.1c12.6-14.5,28.7-23.6,46.7-29.5
                    c14.1-4.6,28.2-9.1,42.5-12.7c29.8-7.5,54.1-23.4,74.3-46.2c2.4-2.7,5-5.4,7.6-8c4.1-4.1,7.6-4.5,10.6,0.4c3,4.9,5.6,10.7,6.3,16.4
                    c1.6,14-4.8,25.8-13.5,35.9c-32.5,37.6-72.4,60.6-123.6,60.2c-1.8,0-3.7,0-5.5,0C77.1,153.5,77,153.9,77,154.2z M55.7,126.5
                    c-7.3,3.5-14.4,7.3-19.5,13.7c-2.7,3.3-0.2,4.9,2.7,5.5c15.5,3.7,31.2,3.9,45.7,3.1c-10.6-1.5-22.5-3-34.2-5c-7-1.2-7.9-4.6-2.9-9.8
                    C50,131.4,52.9,129,55.7,126.5z"/>
                    </svg></a>
                  </p>
              </div>
              <div style="clear:both" class=" initText" >
              </div>

              <p>This blog post shows a few different types of content that's supported and styled with Bootstrap. Basic typography, images, and code are all supported.</p>
              <hr>
              <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>
              <blockquote>
                <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              </blockquote>
              <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
              <h2>Heading</h2>
              <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
              <h3>Sub-heading</h3>
              <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
              <pre><code>Example code block</code></pre>
              <p>Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</p>
              <h3>Sub-heading</h3>
              <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
              <ul>
                <li>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</li>
                <li>Donec id elit non mi porta gravida at eget metus.</li>
                <li>Nulla vitae elit libero, a pharetra augue.</li>
              </ul>
              <p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue.</p>
              <ol>
                <li>Vestibulum id ligula porta felis euismod semper.</li>
                <li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>
                <li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li>
              </ol>
              <p>Cras mattis consectetur purus sit amet fermentum. Sed posuere consectetur est at lobortis.</p>
            </div><!-- /.blog-post -->

            <div class="blog-post">
              <h2 class="blog-post-title">Another blog post</h2>
              <p class="blog-post-meta">December 23, 2013 by <a href="#">Jacob</a></p>

              <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>
              <blockquote>
                <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              </blockquote>
              <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
              <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
            </div><!-- /.blog-post -->

              <footer>
                Publicado no dia 13/04/2016
              </footer>

          </div><!-- /.col-lg-4 -->
          <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 share">
            <a class="face">
              <svg version="1.1" id="Camada_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              	 viewBox="0 0 19.5 19.5" style="enable-background:new 0 0 19.5 19.5;" xml:space="preserve">
              <path class="st0" d="M12,1.8c-0.8,0-1.5,0.2-2.1,0.7C9.2,3,8.8,3.7,8.6,4.6C8.6,4.9,8.6,5.3,8.5,5.7c0,0.6,0,1.1,0,1.7v0.2H6.1v2.8
              	h2.4v7.1h2.9v-7.1h2.4c0.1-0.9,0.2-1.9,0.4-2.8c-0.2,0-0.4,0-0.5,0c-0.7,0-2.2,0-2.2,0s0-1.4,0-2c0-0.8,0.5-1.1,1.2-1.1
              	c0.5,0,1,0,1.5,0c0.1,0,0.1,0,0.2,0V1.9c-0.3,0-0.5-0.1-0.8-0.1C13,1.8,12.5,1.8,12,1.8z"/>
              </svg>
            </a>
            <a class="twitter">
              <svg version="1.1" id="Camada_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              	 viewBox="0 0 19.5 19.5" style="enable-background:new 0 0 19.5 19.5;" xml:space="preserve">
              <path class="st0" d="M16.4,4.9c0.7-0.4,1.1-1,1.4-1.8c0,0,0,0,0,0c0,0,0,0,0,0c-0.6,0.3-1.2,0.6-1.9,0.7c-0.1,0-0.1,0-0.2-0.1
              	c-0.8-0.8-1.8-1.1-2.9-0.9c-1,0.2-1.7,0.7-2.2,1.5C10.1,5,10,5.8,10.1,6.5c0,0.1,0,0.2,0.1,0.4c-2.7-0.2-5-1.3-6.8-3.4
              	C2.9,4.2,2.8,5,3,5.8c0.2,0.8,0.7,1.5,1.4,2c-0.5,0-1-0.2-1.5-0.4c0,0.6,0.2,1.2,0.5,1.7c0.5,0.8,1.2,1.3,2.1,1.5c0,0,0,0,0,0
              	c-0.5,0.1-0.9,0.1-1.4,0c0.3,1,1.3,2.2,3.1,2.3c-0.5,0.4-1,0.7-1.6,0.9c-1,0.4-2.1,0.6-3.3,0.5c0,0,0.1,0,0.1,0.1
              	c1.6,1,3.4,1.5,5.3,1.4c0.6,0,1.2-0.1,1.8-0.2c1.8-0.4,3.3-1.2,4.5-2.5c0.7-0.8,1.3-1.6,1.8-2.6c0.4-0.9,0.7-1.9,0.8-2.9
              	c0.1-0.5,0.1-1,0.1-1.5c0,0,0-0.1,0.1-0.1c0.4-0.3,0.8-0.7,1.2-1.1c0.1-0.2,0.3-0.3,0.4-0.5c0,0,0,0,0,0C17.7,4.7,17,4.8,16.4,4.9z"
              	/>
              </svg>
            </a>
            <a class="link">
              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              	 viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve">
                    <path d="M381,390.368c-11,0-19,8-19,19v19H38v-232h51c11,0,19-8,19-19s-8-19-19-19H19c-11,0-19,8-19,19
              				v270c0,11,8,19,19,19h362c11,0,19-8,19-19v-38C400,398.368,392,390.368,381,390.368z"/>
              			<path d="M503,187.368l-140-140c-13-13-32-2-32,14v62c-73,3-131,27-172,68c-63,63-63,146-63,149
              				c0,10,8,19,19,19c7,0,13-3,16-9c36-64,131-74,184-74h16v64c0,16,20,26,32,14l140-140c4-4,5-9,5-14S507,191.368,503,187.368z
              				 M369,294.368v-35c0-10-7-18-17-19c-1,0-15-2-37-2c-45,0-116,7-169,42c8-20,20-42,40-62c37-37,93-57,164-57c11,0,19-8,19-19v-35
              				l93,93L369,294.368z"/>

              </svg>
            </a>
          </div>

        </div><!-- /.row -->

        <!-- comments -->
        <?php include 'comments-template.php'; ?>
        <!-- end comments -->

        <!-- paginantion -->
        <div class="row pageNext">

          <nav>
            <ul class="pager">
              <li><a href="#" class="btn btn-primary" role="button"><span class="icon back"></span><span class="text">Previous</span></a></li>
              <li><a href="#" class="btn btn-primary" role="button"><span class="icon"></span><span class="text">Next</span></a></li>
            </ul>
          </nav>
        </div>
      </div> <!-- /content -->
      <footer class="principal">

        <div class="coffee">
          <svg version="1.1" id="coffeeSvg" data-morphe="M1280.4,34.7c0,0-132.6-21-287.2-21c-287.2,0-346.2,21-353.2,21c0,0-122-24-434-24c-125.6,0-206,24-206,24V0
	h1280.4V34.7z" data-morphe2="M1280.4,22.7c0,0-132.6,12-287.2,12s-241.3-11-353.2-11s-308.4,11-434,11S0,22.7,0,22.7V0h1280.4V22.7z" data-morphe3="M1280.4,34.7c0,0-132.6-11-287.2-11s-241.3,11-353.2,11s-308.4-14-434-14S0,34.7,0,34.7V0h1280.4V34.7z" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          	 viewBox="0 0 1280 34.7" style="enable-background:new 0 0 1280 34.7;" xml:space="preserve" >
              <path class="st0" d="M1280.4,34.7c0,0-132.6,0-287.2,0s-241.3,0-353.2,0s-308.4,0-434,0s-206,0-206,0V0h1280.4V34.7z"/>
          </svg>
          <svg version="1.1" id="gota" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          	 viewBox="0 0 20 22.8" style="enable-background:new 0 0 20 22.8;" xml:space="preserve">
          <path class="st0" d="M10,1.7c0,0,7.3,8,5.8,13.3C14,21,8,21.7,5.5,18.5C1.9,13.9,10,1.7,10,1.7z"/>
          </svg>

        </div>

        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 text-center">
              <img src="img/footer/cafe.png" alt="" />
              <h3>Assine nossa newsletter</h3>
              <div class="form-center">
                <form class="form-inline">
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">@</div>
                      <label for="exampleInputPassword1">Email</label>
                      <input type="text" class="form-control" id="exampleInputAmount" >
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Assinar</button>
                </form>
              </div>

            </div>


            </div>
            <div class="col-xs-12 col-sm-12 copyright text-center ">© 2016-2017 Grano Studio</div>
          </div>
        </div>
      </footer>
    </div> <!-- /content move open menu -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- end bootstrap js -->
    <script src="js/plugins.min.js"></script>
    <script src="js/ui.min.js"></script>
    <!-- <script src="../../assets/js/docs.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
  </body>
</html>
