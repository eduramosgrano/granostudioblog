<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'granostudio13');

/** MySQL database username */
define('DB_USER', 'granostudio13');

/** MySQL database password */
define('DB_PASSWORD', 'q1w2e3zaxscd');

/** MySQL hostname */
define('DB_HOST', '179.188.16.32');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#X?s=;HCtYfr*w+WL_H]k=<eat.E9cTNXgxjy:J73l&_]C`ucLu-qBg`/B.l7X%3');
define('SECURE_AUTH_KEY',  'ehG`?1-[a|2%g5+{ZK$dtkx-U-l9P+)fA!8Z6U}|+6/O0Q6RL@BNm)/apD:%YWV|');
define('LOGGED_IN_KEY',    '5|[Hn2W^-j-)W+wx8/ZLF|9Y@bc+aqsfiOZLl-r;X]}B(Q^=OMY(t`0jdD-%ebP#');
define('NONCE_KEY',        'J[Bw|>yA-jn:|+9Hj7)Lgr+-N@LJAX<co^B^!}?+|DgxF5+P#L}KN~m-.d}q&njF');
define('AUTH_SALT',        'G2-?!# {};/wCC8gX}e(>:IlIsLo?jju9dI]Lk*5sWSv%fF+c{r]vt=.zL326<iK');
define('SECURE_AUTH_SALT', '<j4nk;rqvg=cU-FZ95<]aW.rWhPtU>t{D`Y-%(&LN+#1XJw^G=U&h/Fr)TL]r,8a');
define('LOGGED_IN_SALT',   '`6tUjY&%7Y= lElJgs0vEWD+pf6u|xz-a3{]y(&38|%a{iQc#,8J-M8|P>T!||_t');
define('NONCE_SALT',       'BJ>ju]w8JaF>VD+>S>C7BEYP4UVGp$Zm0EMXUaS<wXq* _V4=lo4h~]F&%)@5BOm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
