

// Menu



var bodyEl   = jQuery( 'body' ),
    content  = jQuery( '.content-wrap' ),
    openbtn  = jQuery( '#open-button' ),
    closebtn = jQuery( '#close-button' ),
    closebtnwd = jQuery( '#close-widget' ),
    isOpen   = false,
    isSearch = false,
    search   = jQuery( '.menu-search' ),
    menuWrap = jQuery( '.menu-wrap'),

    openbtnwd= jQuery('#open-button-widget'),
    openhvwd = jQuery('#open-hover-widget'),
    isOpenWd = false,
    morphEl = $( '#morph-shape' );
    morphElWd = $( '.widget-wrap #morph-shape' );

var s = Snap( '#morph-shape svg' );
var path = s.select( 'path' );
    initialPath = path.attr('d'),
    pathOpen = morphEl.data( 'morph-open' ),
    isAnimating = false;
    isAnimatingWd = false;
var sWd = Snap( '.widget-wrap #morph-shape svg' );
var pathWd = sWd.select( 'path' );
    initialPathWd = pathWd.attr('d'),
    pathOpenWd = morphElWd.data( 'morph-open' );

// footer
var footerCoffee = Snap( '#coffeeSvg' );
var CoffeePath = footerCoffee.select( 'path' ),
    CoffeePathInitPath = CoffeePath.attr('d'),
    CoffeePathAni = footerCoffee.attr( 'data-morphe' ),
    CoffeePathAni2 = footerCoffee.attr( 'data-morphe2' ),
    CoffeePathAni3 = footerCoffee.attr( 'data-morphe3' ),
    footerAniEnd = false,
    footergota = jQuery('#gota');

// scroll to top
var scrollbtn = jQuery( '.totop' );

jQuery(document).scroll(function(event) {
  /* Act on the event */
  var bodyscrolltop = $('body').scrollTop();
  if( bodyscrolltop >= 3){
      scrollbtn.addClass('active');
  }else{
      scrollbtn.removeClass('active');
  }

  if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
    if(!footerAniEnd){
      CoffeePath.animate({'path': CoffeePathAni}, 600, mina.easein, function(){
        footergota.addClass('active');
        CoffeePath.animate({'path': CoffeePathAni2}, 600, mina.backout, function(){
          CoffeePath.animate({'path': CoffeePathAni3}, 600, mina.easein, function(){
            CoffeePath.animate({'path': CoffeePathInitPath}, 600, mina.easein, function(){
              footerAniEnd = true;
            })
          })
        })
      });
    }
   }

});

function toTop(){
  $("html, body").animate({ scrollTop: 0 }, 600);
  scrollbtn.removeClass('active');
  return false;
};

scrollbtn.on('click',toTop);


// inputs
var formGroup = jQuery('.form-group'),
    inputs = formGroup.find('input'),
    label  = formGroup.find('label');

inputs.focusin(activeForm);
inputs.focusout(toggleForm);

    function activeForm(){
          $(this).parent().find('label').addClass('active');
          if($(this).parent().find('.input-group-addon')){
            $(this).parent().find('.input-group-addon').addClass('active');
          }
    }

    function toggleForm(){
        if(!$(this).val()){
            $(this).parent().find('label').removeClass('active');
            if($(this).parent().find('.input-group-addon')){
              $(this).parent().find('.input-group-addon').removeClass('active');
            }
        }
    }



    // alert(CoffeePath);



jQuery(document).ready(function($) {

      // hode mensagem de lodaing jquery mobile
      $.mobile.loading().hide();

    	function init() {
    		initEvents();
    	}

      function initEvents() {

    		openbtn.on( 'click', toggleMenu );

        menuWrap.on("swipeleft", toggleMenu );
        // widget
        openbtnwd.on('click', toggleMenuWd);
        $('.widget-wrap').on("swiperight", toggleMenuWd );

        openhvwd.hover(toggleMenuWd);

        search.on('click', openSearch);

    		if( closebtn ) {
            closebtn.on( 'click', function(){
              if(isSearch){
                closeSearch();
              }else{
                toggleMenu();
              }
            });
    		}
        if( closebtnwd ) {
    			closebtnwd.on( 'click', toggleMenuWd );
    		}

        content.hover(function(){
          if(isOpenWd){
            toggleMenuWd();
          }
        });

    		// close the menu element if the target it´s not the menu element or one of its descendants..
    		content.on( 'click', function() {
      			if( isOpen ) {
      				toggleMenu();
      			}
            if( isOpenWd ) {
      				toggleMenuWd();
      			}
    		  } );
    	}


      function openSearch(){

        if(isSearch==false){
          toTop();
          menuWrap.addClass( 'search-active' );
          search.find('input').focus();
          isSearch = true;
        }

      }
      function closeSearch(){
          menuWrap.removeClass( 'search-active' );
          search.find('input').val('');
          isSearch = false;
      }

      function toggleMenu() {
    		if( isAnimating ) return false;
    		isAnimating = true;
    		if( isOpen ) {
    		  $('body').removeClass( 'show-menu' );
    			// animate path
    			setTimeout( function() {
    				// reset path
    				path.attr( 'd', initialPath );
    				isAnimating = false;
    			}, 300 );
    		}
    		else {
    			$('body').addClass( 'show-menu' );
    			// animate path
    			path.animate( { 'path' : pathOpen }, 400, mina.easeinout, function() { isAnimating = false; } );
    		}
    		isOpen = !isOpen;
    	}

      // widget
      function toggleMenuWd() {
    		if( isAnimatingWd ) return false;
    		isAnimatingWd = true;
    		if( isOpenWd ) {
    		  $('body').removeClass( 'show-widget' );
    			// animate path
    			setTimeout( function() {
    				// reset path
    				pathWd.attr( 'd', initialPathWd );
    				isAnimatingWd = false;
    			}, 300 );
    		}
    		else {
    			$('body').addClass( 'show-widget' );
    			// animate path
    			pathWd.animate( { 'path' : pathOpenWd }, 400, mina.easeinout, function() { isAnimatingWd = false; } );
    		}
    		isOpenWd = !isOpenWd;
    	}

      init();

});
// END Menu
